# Generate simulation output ---------------------------------------------------
#' Generate simulation output
#'
#' Generate summary statistics of the current state of the simulation. N.B., all
#' individuals belonging to a cluster or supercluster are assumed to be next to
#' each other in each vector or matrix column and to be ordered exactly the same
#' in all input vectors and matrix columns!
#'
#' @param x Logical matrix of having susceptible (first column), exposed,
#'   infectious or recovered status (last column).
#' @param cluster Integer vector of cluster memberships.
#' @param supercluster Integer vector of supercluster memberships.
#' @param rel_contact_rate Numerical vector of individual relative contact
#'   rates (not accounting for effects of interventions).
#' @param t Current time in the simulation.
#' @param output_quick Logical flag indicating whether only the bare minimum
#'   amount of output should be generated (i.e. number of people in each of the
#'   SEIR compartments).
#' @param output_cluster Logical flag indicating whether cluster-level output
#'   should be generated. If not, output will be aggregated to
#'   supercluster-level.
#' @param ... Arguments passed down via \code{...} in the \code{virsim}
#'   function.
#'
#' @return Returns a \code{data.table} with the following summary statistics:
#' \describe{
#'   \item{\code{time}}{Time (day) in the simulation.}
#'   \item{\code{S, E, I, R}}{Absolute number of susceptible, exposed, infected,
#'     and recovered (or dead/removed) individuals.}
#'   \item{\code{inc}}{Then number of cases that entered compartment I since the
#'     previous time point in the summary output.}
#'   \item{\code{contact_mu_S, contact_mu_E, contact_mu_I, contact_mu_R}}{The
#'     average relative contact rate of individuals in each the compartments S,
#'     E, I, and R.}
#' }
#'
#' @importFrom matrixStats colCumsums colDiffs
#'
#' @export
gen_output <- function(x,
                       cluster,
                       supercluster,
                       rel_contact_rate,
                       t,
                       output_quick = FALSE,
                       output_cluster = TRUE,
                       ...) {

  if (output_cluster) {
    index <- c(cluster[-length(cluster)] != cluster[-1L], TRUE)

    which_index <- which(index)

    o1 <- structure(colDiffs(rbind(0, colCumsums(x)[index, ])),
                    dimnames = list(NULL, c("S", "E", "I", "R")))
    if (!output_quick) {
      o2 <- structure(colDiffs(rbind(0, colCumsums(rel_contact_rate * x)[index, ])),
                      dimnames = list(NULL, c("contact_mu_S",
                                              "contact_mu_E",
                                              "contact_mu_I",
                                              "contact_mu_R")))
      data.table(supercluster = supercluster[which_index],
                 cluster = cluster[which_index],
                 time = t,
                 o1,
                 o2 / o1)
    } else {
      data.table(supercluster = supercluster[which_index],
                 cluster = cluster[which_index],
                 time = t,
                 o1)
    }

  } else {
    index <- c(supercluster[-length(supercluster)] != supercluster[-1L], TRUE)
    which_index <- which(index)

    o1 <- structure(colDiffs(rbind(0, colCumsums(x)[index, ])),
                    dimnames = list(NULL, c("S", "E", "I", "R")))
    if (!output_quick) {
      o2 <- structure(colDiffs(rbind(0, colCumsums(rel_contact_rate * x)[index, ])),
                      dimnames = list(NULL, c("contact_mu_S",
                                              "contact_mu_E",
                                              "contact_mu_I",
                                              "contact_mu_R")))
      data.table(supercluster = supercluster[which_index],
                 time = t,
                 o1,
                 o2 / o1)
    } else {
      data.table(supercluster = supercluster[which_index],
                 time = t,
                 o1)
    }
  }
}


# Auxiliary function to quickly calculate differences in a vector without checks
calc_diff <- function(X) {
  X[-1L] - X[-length(X)]
}


# Generate derived incidence and/or prevalence ---------------------------------
#' Generate derived incidence and/or prevalence
#'
#' Generate a time series of derived incident and prevalent cases, e.g. new IC
#' admissions and number of cases currently in IC, given the simulated incidence
#' of infectious cases in \code{virsim}, the probability of the outcome
#' occurring among infectious cases, the distribution of duration until
#' occurrence from start of infectiousness, and the distribution of duration of
#' the outcome. To generate derived output based on time-varying parameters,
#' consider using \code{\link{gen_param_compound}} to generate a list of
#' time-varying parameter values that can be directly fed into
#' \code{gen_derived_outcome}.
#'
#' @param inc A vector of time-specific incidences (number of infectious cases).
#' @param time A vector of unique time points (days) of equal length as
#'   \code{inc}..
#' @param prob The probability of a derived outcome occurring. If this is a
#'   vector, it should of equal length as arguments \code{inc} and \code{time}
#'   and will be interpreted as a time-varying probability.
#' @param delay_distr A named list describing the distribution of delay until
#'   occurrence of the derived outcome. The first element ("name") indicates the
#'   name of the distribution to be used. E.g., if \code{name = "newdistr"} is
#'   specified, the function \code{rnewdistr()} will be called to generate
#'   durations. The other named elements of the list will be used as arguments
#'   to the distribution function (if these are of length >1, these should be of
#'   equal length as \code{inc} and \code{time}).
#' @param dur_distr A named list describing the distribution of duration of the
#'   derived outcome The first element ("name") indicates the name of the
#'   distribution to be used. E.g., if \code{name = "newdistr"} is specified,
#'   the function \code{rnewdistr()} will be called to generate durations. The
#'   other named elements of the list will be used as arguments for the
#'   distribution function.
#' @param n_draw Number of draws to perform. If \code{NULL}, one draw is
#'   performed. If more than one draw is performed, the mean is returned as a
#'   point estimate.
#' @param CI_probs Percentiles for confidence intervals to be calculated. Note
#'   that generating CIs may take considerable time, depending on the number
#'   draws as specified by \code{n_draw}. If \code{n_draw == NULL},
#'   \code{CI_probs} will be ignored.
#' @param ... Ignored.
#'
#' @return Returns a data.table of incident and prevalent IC cases; if CI_probs
#'   is specified, additional columns are returned.
#'
#' import data.table
#'
#' @importFrom matrixStats rowMeans2 rowQuantiles
#' @export
gen_derived_outcome <- function(inc,
                                time,
                                prob = 1 / 182,
                                delay_distr = list(name = "weibull",
                                                   shape = 10,
                                                   scale = exp(log(14) -
                                                                 lgamma(1 + 1 / 10))),
                                dur_distr = list(name = "weibull",
                                                 # based on NICE data up to 19 May 2020
                                                 shape = 1.24,
                                                 scale = exp(log(19.8) -
                                                               lgamma(1 + 1 / 1.24))),
                                n_draw = NULL,
                                CI_probs = NULL,
                                ...) {

  if (length(unique(time)) != length(inc)) {
    stop("'inc' and 'time' must be of equal length and 'time' must contain unique values")
  }
  if (length(prob) > 1 && length(prob) != length(inc)) {
    stop("if length(prob) > 1, i must of be of equal length as 'inc'")
  }

  # Generate number of outcomes and their time points of occurrence
  if (is.null(n_draw)) {

    n_outcome <- rbinom(n = length(inc), size = inc, p = prob)
    n <- sum(n_outcome)
    start_I <- unlist(mapply(rep, x = time, times = n_outcome))

    # If time-changing parameters are specified, correctly specify vector of
    # input parameter for delay distribution
    for (i in 1:length(delay_distr[-1])) {
      if (length(delay_distr[-1][[i]]) > 1) {
        delay_distr[-1][[i]] <- approx(x = time, y = delay_distr[-1][[i]],
                                       xout = start_I,
                                       method = "constant", rule = 2, f = 0)$y
      }
    }

    start_outcome <- start_I + do.call(what = paste0("r", delay_distr$name),
                                       args = c(list(n = n), delay_distr[-1]))

    # If time-changing parameters are specified, correctly specify vector of
    # input parameter for duration distribution
    for (i in 1:length(dur_distr[-1])) {
      if (length(dur_distr[-1][[i]]) > 1) {
        dur_distr[-1][[i]] <- approx(x = time, y = dur_distr[-1][[i]],
                                     xout = start_outcome,
                                     method = "constant", rule = 2, f = 0)$y
      }
    }

    end_outcome <- start_outcome + do.call(what = paste0("r", dur_distr$name),
                                           args = c(list(n = n), dur_distr[-1]))
    as.data.table(structure(inc_prev(time, start_outcome, end_outcome),
                            dimnames = list(NULL, c("inc", "prev"))))

  } else {

    temp <- matrix(0, nrow = length(time) * 2, ncol = n_draw)

    for (j in 1:n_draw) {
      n_outcome <- rbinom(n = length(inc), size = inc, p = prob)
      n <- sum(n_outcome)
      start_I <- unlist(mapply(rep, x = time, times = n_outcome))

      # If time-changing parameters are specified, correctly specify vector of
      # input parameter for delay distribution
      delay_distr_temp <- delay_distr
      for (i in 1:length(delay_distr_temp[-1])) {
        if (length(delay_distr_temp[-1][[i]]) > 1) {
          delay_distr_temp[-1][[i]] <- approx(x = time,
                                              y = delay_distr_temp[-1][[i]],
                                              xout = start_I,
                                              method = "constant",
                                              rule = 2, f = 0)$y
        }
      }

      start_outcome <- start_I + do.call(what = paste0("r", delay_distr$name),
                                         args = c(list(n = n), delay_distr[-1]))

      # If time-changing parameters are specified, correctly specify vector of
      # input parameter for duration distribution
      dur_distr_temp <- dur_distr
      for (i in 1:length(dur_distr[-1])) {
        if (length(dur_distr[-1][[i]]) > 1) {
          dur_distr_temp[-1][[i]] <- approx(x = time, y = dur_distr[-1][[i]],
                                            xout = start_outcome,
                                            method = "constant", rule = 2, f = 0)$y
        }
      }

      end_outcome <- start_outcome + do.call(what = paste0("r", dur_distr_temp$name),
                                             args = c(list(n = n), dur_distr_temp[-1]))
      temp[, j] <- inc_prev(time, start_outcome, end_outcome)
    }

    if (!is.null(CI_probs)) {
      as.data.table(cbind(matrix(matrixStats::rowMeans2(temp),
                                 nrow = length(time),
                                 dimnames = list(NULL, c("inc", "prev"))),
                          matrix(matrixStats::rowQuantiles(temp, probs = CI_probs),
                                 nrow = length(time),
                                 dimnames = list(NULL,
                                                 sapply(CI_probs,
                                                        function(x)
                                                          paste0(c("inc_", "prev_"), x))))))
    } else {
      as.data.table(matrix(matrixStats::rowMeans2(temp),
                           nrow = length(time),
                           dimnames = list(NULL, c("inc", "prev"))))
    }
  }
}


# Create a time-varying parameter object for derived inc/prev generation ----
#' Create a time-varying parameter object for derived inc/prev generation
#'
#' Create a time-varying parameter object for generation of derived incidence
#' and/or prevalence output using \code{\link{gen_derived_outcome()}}.
#'
#' @param param_list A list of parameter lists, each consisting of three
#'   elements: \code{prob}, \code{delay_distr}, and \code{dur_distr}. Each of
#'   these three elements have to be specified in accordance with the
#'   requirements for \code{\link{gen_derived_outcome()}}, with the special note
#'   that each parameter list should represent a parameter set that does not
#'   vary over time (i.e. no vectors allowed!).
#' @param dur_vector An integer vector of equal length as \code{param_list}
#'   indicating the duration of the period that each parameter list within
#'   \code{param_list} should be applied to.
#'
#' @return Returns a parameter list object with time-varying parameter values,
#'   organised in three elements: \code{prob}, \code{delay_distr}, and
#'   \code{dur_distr} which can be used as arguments to
#'   \code{\link{gen_derived_outcome()}}.
#'
#' @export
gen_param_compound <- function(param_list, dur_vector) {

  if (!is.list(param_list))
    stop("'param_list' must dur_list a list")
  if (!is.numeric(dur_vector))
    stop("'dur_vector' must be a numeric vector")
  if (length(param_list) != length(dur_vector))
    stop("'param_list' and 'dur_vector' must be of equal length")
  if (any(sapply(unlist(unlist(param_list,
                               recursive = FALSE),
                        recursive = FALSE),
                 length) > 1))
    stop("parameter lists within 'param_list' may only contain single values")

  param_compound <- param_list[[1]]

  param_compound$prob <-
    unlist(mapply(FUN = rep,
                  x = lapply(param_list, function(x) x$prob),
                  time = dur_vector))

  for (i in c("dur_distr", "delay_distr")) {
    temp <- param_compound[[i]][-1]
    names_temp <- names(temp)
    for (j in 1:length(temp)) {
      temp[[j]] <-
        unlist(mapply(FUN = rep,
                      x = lapply(param_list, function(x) x[[i]][-1][[j]]),
                      time = dur_vector))
    }
    param_compound[[i]][-1] <- temp
    rm(temp, names_temp)
  }

  param_compound

}


# Aggregate simulation output --------------------------------------------------
#' Aggregate simulation output over whole population
#'
#' Aggregate summary statistics of the current state of the simulation over
#' clusters and superclusters.
#'
#' @param x Output object created by \code{gen_output}.
#'
#' @export
aggregate_output <- function(x) {

  y <- x[, lapply(.SD, sum),
         by = time,
         .SDcols = c("S", "E", "I", "R", "inc")]

  if (any("contact_mu_S" %in% names(x))) {
    y[, c("contact_mu_S", "contact_mu_E", "contact_mu_I", "contact_mu_R") :=
        x[, .(weighted.mean(contact_mu_S, S, na.rm = TRUE),
              weighted.mean(contact_mu_E, E, na.rm = TRUE),
              weighted.mean(contact_mu_I, I, na.rm = TRUE),
              weighted.mean(contact_mu_R, R, na.rm = TRUE)),
          by = time][, .(V1, V2, V3, V4)]][]
  }

  y

}

# Aggregate simulation output per supercluster ---------------------------------
#' Aggregate simulation output per supercluster
#'
#' Aggregate summary statistics of the current state of the simulation over
#' clusters per supercluster.
#'
#' @param x Output object created by \code{gen_output}.
#'
#' @import data.table
#'
#' @export
aggregate_output_sc <- function(x) {

  y <- x[, lapply(.SD, sum),
         by = .(supercluster, time),
         .SDcols = c("S", "E", "I", "R", "inc")]

  if (any("contact_mu_S" %in% names(x))) {
    y[, c("contact_mu_S", "contact_mu_E", "contact_mu_I", "contact_mu_R") :=
        x[, .(weighted.mean(contact_mu_S, S, na.rm = TRUE),
              weighted.mean(contact_mu_E, E, na.rm = TRUE),
              weighted.mean(contact_mu_I, I, na.rm = TRUE),
              weighted.mean(contact_mu_R, R, na.rm = TRUE)),
          by = .(supercluster, time)][, .(V1, V2, V3, V4)]][]
  }

  y

}


# Generate axis ticks for a logarithmic axis ----
base_breaks <- function(n = 10) {
  function(x) {
    axisTicks(log10(range(x, na.rm = TRUE)), log = TRUE, n = n)
  }
}


# Generate random x and y coordinates uniformly distributed within a circle  ----
gen_circle_xy <- function(n, c1 = .5, c2 = .25, iter = 100, even_space = FALSE) {

  xy <- lhs::maximinLHS(n = n, k = 2)
  theta <- xy[, 2] * 2 * pi
  rr <- sqrt(xy[, 1])
  xy <- cbind(x = rr * cos(theta),
              y = rr * sin(theta))

  if (even_space == TRUE) {
    # Create even space between points
    dxy <- xy
    for (i in 1:iter) {

      dxy[] <- 0
      d_euclid <- as.matrix(dist(xy, method = "euclidean"))

      for (j in 1:nrow(xy)) {
        # for (k in 1:nrow(xy)) {
        #   if (d_euclid[j, k] != 0) {
        #     dxy[j, ] <- dxy[j, ] - (xy[k, ] - xy[j, ]) / d_euclid[j, k]^2
        #   }
        # }
        ## Same:
        dxy[j, ] <- apply(t(xy[j, ] - t(xy[-j, ])) / d_euclid[j, -j]^2, 2, sum)
      }

      xy <- xy + dxy * c2 / (nrow(xy) - 1) - c1 * xy
    }
  }

  return(xy)
}


# Print a png to disk for generation of an animated gif ----
create_png <- function(pop, time) {
  bounds <- pop[, max(diff(range(x)), diff(range(y)))] / 2
  print(
    ggplot(mapping = aes(x = x, y = y,
                         # size = rel_contact_rate)) +
                         size = rel_cr_eff)) +
      geom_point(data = data.table(x = 0, y = 0,
                                   rel_contact_rate = c(0, pop[, max(rel_contact_rate)]),
                                   rel_cr_eff = c(0, pop[, max(rel_contact_rate)])),
                 alpha = 0) +
      geom_point(data = pop[(susceptible) & rel_contact_rate == rel_cr_eff],
                 shape = 1, col = "grey", alpha = 0.7) +
      geom_point(data = pop[(susceptible) & rel_contact_rate != rel_cr_eff],
                 shape = 1, col = "grey", alpha = 0.4) +
      # geom_point(data = pop[(susceptible)], shape = 1, col = "grey") +
      geom_point(data = pop[(recovered)], alpha = 0.4, col = "blue") +
      geom_point(data = pop[(exposed)], alpha = 0.5, col = "orange") +
      geom_point(data = pop[(infectious)], alpha = 0.5, col = "red") +
      scale_radius(guide = FALSE, range = c(1, 5)) +
      theme(panel.grid = element_blank(),
            axis.title = element_blank(),
            axis.text = element_blank(),
            axis.ticks = element_blank(),
            panel.background = element_blank()) +
      labs(title = paste0("Day ", time),
           subtitle = paste0("Current infections: ",
                             pop[state %in% c("E", "I"),
                                 format(.N, big.mark = ",")],
                             "\nRecovered (immune): ",
                             format(pop[state == "R", .N] / pop[, .N] * 100,
                                    digits = 1,
                                    nsmall = 1),
                             "%")) +
      expand_limits(x = c(-bounds, bounds),
                    y = c(-bounds, bounds))
  )
}


# Create a cluster-level animated gif ----
#' Create a cluster-level animated GIF
#'
#' Create a cluster-level animated GIF
#'
#' @param x Simulation output (element "monitor" produced by \code{virsim}).
#' @param gif_width GIF width (inches).
#' @param gif_height GIF heigth (inches).
#' @param gif_dpi GIF dots per inch (dpi).
#' @param gif_speed Frame speed (lower values = faster).
#' @param gif_sc_spacing Relative distance between centers of superclusters in
#'   plot.
#' @param gif_name Name of GIF file (without extension).
#' @param max_E_scale Proportion of population in E for which opacity (alpha) is
#'   at maximum value (\code{max_E_alpha}).
#' @param max_I_scale Proportion of population in I for which opacity (alpha) is
#'   at maximum value (\code{max_I_alpha}).
#' @param max_R_scale Proportion of population in R for which opacity (alpha) is
#'   at maximum value  (\code{max_R_alpha}).
#' @param max_E_alpha Highest possible opacity for E.
#' @param max_I_alpha Highest possible opacity for I.
#' @param max_R_alpha Highest possible opacity for R.
#' @param x_include Values to be included (guaranteed) of the x-axis of the plot
#'   of \code{type = "immunity"}.
#' @param size_breaks Vector of population size to be displayed in the legend.
#' @param size_max Maximum size of symbol for highest population size.
#' @param type Character values for the type of visualisation: "transmission" or
#'   "immunity".
#' @param caption_size Size (in points) of the caption below the figure.
#' @param strategy Character value with description of the strategy. This
#'   information is concatenates in the caption with the statement " in a
#'   population of ... across ... clusters in ... superclusters" (with
#'   auto-populated numbers).
#' @param theme_choice Ggplot theme used to format frames for the "immunity"
#'   type GIF. Specify as e.g. \code{theme_choice = theme_classic()}.
#'
#' @return Creates an animated GIF on disk.
#'
#' @import data.table
#'
#' @export
create_gif_cluster <- function(x,
                               gif_width = 8,
                               gif_height = 8,
                               gif_dpi = 300,
                               gif_speed = 20,
                               gif_sc_spacing = 6,
                               gif_name = "virsim",
                               max_E_scale = .005,
                               max_I_scale = .1,
                               max_R_scale = .85,
                               max_E_alpha = .5,
                               max_I_alpha = .8,
                               max_R_alpha = .8,
                               x_include = 1,
                               size_breaks = c(1e4, 1e3, 1e2),
                               size_max = 6,
                               type = "transmission",
                               caption_size = 10,
                               strategy = "Natural course",
                               theme_choice = theme_linedraw()) {

  if (!any(type %in% c("transmission", "immunity"))) {
    stop("type must be 'transmission' or 'immunity'")
  }

  # Transform data to proportions
  x[, N := S + E + I + R]
  x[, c("S", "E", "I", "R") := lapply(.SD, function(x) x / N),
    .SDcols = c("S", "E", "I", "R")]

  # Calculate mean relative contact rate per cluster
  x[, c("contact_mu_S", "contact_mu_E", "contact_mu_I", "contact_mu_R") :=
      lapply(.SD, function(var) {
        var[is.na(var)] <- 0
        var
      }),
    .SDcols = c("contact_mu_S", "contact_mu_E", "contact_mu_I", "contact_mu_R")]
  x[, w_mean := S * contact_mu_S + E * contact_mu_E + I * contact_mu_I + R * contact_mu_R]

  # Scale variabels to correctly plot translucency / opaqueness.
  x[, E_plot := E / max_E_scale]
  x[, I_plot := I / max_I_scale]
  x[, R_plot := R / max_R_scale]
  x[, c("E_plot", "I_plot", "R_plot") := lapply(.SD, function(x) ifelse(x > 1, 1, x))
    , .SDcols = c("E_plot", "I_plot", "R_plot")]
  x[, E_plot := E_plot * max_E_alpha]
  x[, I_plot := I_plot * max_I_alpha]
  x[, R_plot := R_plot * max_R_alpha]

  # Add coordinates for clusters, clustered by supercluster
  x[, c("X_sc", "Y_sc") :=
      as.data.table(gen_circle_xy(n = length(unique(supercluster)),
                                  even_space = TRUE))[supercluster]]
  x[, c("X_cl", "Y_cl") :=
               as.data.table(gen_circle_xy(n = length(unique(cluster))))[as.numeric(factor(cluster))],
             by = supercluster]

  x[, X := (X_sc * gif_sc_spacing + X_cl) / (gif_sc_spacing)]
  x[, Y := (Y_sc * gif_sc_spacing + Y_cl) / (gif_sc_spacing)]

  x[, X := X - mean(range(X))]
  x[, Y := Y - mean(range(Y))]

  x[, c("X_sc", "Y_sc", "X_cl", "Y_cl") := NULL]

  # Print PNG to disk for every time point
  print("Creating PNG file per time point")
  png(file = paste0(gif_name, "_%05d.png"),
      width = gif_width,
      height = gif_height,
      res = gif_dpi,
      unit = "in")

  n_pop <- x[time == min(time), sum(N)]
  n_cluster <- x[, length(unique(cluster))]
  n_supercluster <- x[, length(unique(supercluster))]

  caption_plot <- paste0(strategy,
                         " in a population of ", format(n_pop, big.mark = ","),
                         " across ", format(n_cluster, big.mark = ","),
                         " clusters and ", format(n_supercluster, big.mark = ","),
                         " superclusters.")

  if (type == "transmission") {
    bounds <- x[, max(diff(range(X)), diff(range(Y)))] / 2

    for (t in x[, unique(time)]) {
      print(
        ggplot(data = x[time == t],
               mapping = aes(x = X, y = Y, size = N)) +
          geom_point(data = data.table(X = 0, Y = 0,
                                       N = 0, dummy = c(0, 1)),
                     mapping = aes(alpha = dummy),
                     col = "white", shape = 1) +
          geom_point(col = "darkgrey", shape = 1) +
          geom_point(mapping = aes(alpha = R_plot,
                                   col = paste0("Recovered: ",
                                                format(sum(R * N),
                                                       big.mark = ",")))) +
          geom_point(mapping = aes(alpha = E_plot,
                                   col = paste0("Exposed: ",
                                                format(sum(E * N),
                                                       big.mark = ",")))) +
          geom_point(mapping = aes(alpha = I_plot,
                                   col = paste0("Infectious: ",
                                                format(sum(I * N),
                                                       big.mark = ",")))) +
          scale_size_area(name = paste0("Population size"),
                          max_size = size_max,
                          breaks = size_breaks,
                          guide = guide_legend(override.aes = list(shape = 1,
                                                                   col = "darkgrey"),
                                               order = 2)) +
          scale_alpha_continuous(guide = FALSE, range = c(0, 1)) +
          scale_colour_manual(name = paste0("Day ", t),
                              values = c("orange", "red", "blue"),
                              guide = guide_legend(override.aes = list(alpha = 0.4,
                                                                       shape = 21,
                                                                       fill = c("orange", "red", "blue"),
                                                                       col = "darkgrey",
                                                                       size = 4),
                                                   order = 1)) +
          labs(caption = caption_plot) +
          theme(panel.grid = element_blank(),
                axis.title = element_blank(),
                axis.text = element_blank(),
                axis.ticks = element_blank(),
                panel.background = element_blank(),
                legend.position = c(0, 1),
                legend.justification = c(.025, .975),
                legend.background = element_blank(),
                legend.key = element_rect(fill = NA),
                plot.caption = element_text(color = "darkgrey",
                                            face = "italic",
                                            size = caption_size,
                                            hjust = 0.5)) +
          expand_limits(x = c(-bounds, bounds),
                        y = c(-bounds, bounds))
      )
    }
  }

  if (type == "immunity") {

    for (t in x[, unique(time)]) {
      print(
        ggplot(data = x[time == t],
               mapping = aes(x = w_mean, y = R * 100, size = N)) +
          theme_choice +
          geom_point(data = data.table(w_mean = 1, R = 0,
                                       N = 0, dummy = c(0, 1)),
                     mapping = aes(alpha = dummy),
                     col = "white", shape = 1) +
          geom_point(col = "darkgrey", shape = 1) +
          geom_point(mapping = aes(alpha = R_plot,
                                   col = paste0("Recovered: ",
                                                format(sum(R * N),
                                                       big.mark = ",")))) +
          geom_point(mapping = aes(alpha = E_plot,
                                   col = paste0("Exposed: ",
                                                format(sum(E * N),
                                                       big.mark = ",")))) +
          geom_point(mapping = aes(alpha = I_plot,
                                   col = paste0("Infectious: ",
                                                format(sum(I * N),
                                                       big.mark = ",")))) +
          scale_x_log10(name = "\nAverage relative contact rate",
                        breaks = base_breaks(n = 10)) +
          scale_y_continuous(name = "Percentage of population recovered / immune (%)\n",
                             breaks = 0:5 * 20) +
          scale_size_area(name = paste0("Population size"),
                          max_size = size_max,
                          breaks = size_breaks,
                          guide = guide_legend(override.aes = list(shape = 1,
                                                                   col = "darkgrey"),
                                               order = 2)) +
          scale_alpha_continuous(guide = FALSE, range = c(0, 1)) +
          scale_colour_manual(name = paste0("Day ", t),
                              values = c("orange", "red", "blue"),
                              guide = guide_legend(override.aes = list(alpha = 0.4,
                                                                       shape = 21,
                                                                       fill = c("orange", "red", "blue"),
                                                                       col = "darkgrey",
                                                                       size = 4),
                                                   order = 1)) +
          labs(caption = caption_plot) +
          theme(legend.position = c(0, 1),
                legend.justification = c(-.05, 1.05),
                legend.key = element_rect(fill = NA),
                legend.background = element_rect(colour = 'black',
                                                 fill = 'white',
                                                 linetype = 'solid',
                                                 size = .25),
                plot.caption = element_text(color = "darkgrey",
                                            face = "italic",
                                            size = caption_size,
                                            hjust = 0.5)) +
          expand_limits(x = x_include, y = c(0, 100))
      )
    }
  }

  # Clean up and create compile GIF from PNG files
  dev.off()
  print("Creating animated GIF")
  system(paste0("convert -delay ", gif_speed,
                " *.png -loop 1 '", gif_name, ".gif'"))
  file.remove(list.files(pattern = ".png"))

}


### END OF CODE ### ----
