# Update contact rates ---------------------------------------------------------
#' Update contact rate
#'
#' \code{update_contact} updates agent contact rates as a function of timing of
#' interventions and individual agent uptake of interventions.
#'
#' @param pop \code{data.table} object containing the state of the population.
#' @param t Time in days.
#' @param intervention_t A vector of time points at which contact rate-related
#'   interventions change (days) anywhere in the whole population.
#' @param intervention_uptake A matrix of time-specific values (columns) for the
#'   proportion of people that take up contact rate-related intervention. Uptake
#'   is simulated assuming piece-wise constant uptake over time. Each row
#'   represents a supercluster.
#' @param intervention_effect A matrix of time-specific effect of interventions
#'   (i.e. a multiplier) on individual host contact rate in case of uptake (1 =
#'   no effect, 0 = max effect). Each row represents a supercluster.
#' @param intervention_effect_var Measure of inter-individual variation in
#'   effect of intervention in case of uptake. NB: variation increases for lower
#'   values of \code{intervention_effect_var}, which is the sum of the shape
#'   parameters of a beta distribution. If \code{intervention_effect_var = Inf},
#'   all individuals who take up the interventions experience the same effect.
#' @param ... Arguments passed down via \code{...} in the \code{virsim}
#'   function.
#'
#' @return This function does not return anything and relies on side-effects via
#'   the \code{data.table} structure.
#'
#' @import data.table
#'
#' @export
update_contact_rate <- function(pop,
                                t,
                                intervention_t,
                                intervention_uptake,
                                intervention_effect,
                                intervention_effect_var,
                                ...) {


  # Update uptake and effect of interventions
  pop[, effect := 1]
  time_index <- findInterval(t, intervention_t)

  if (intervention_effect_var == Inf) {

    pop[uptake_p <= intervention_uptake[IU, time_index],
        effect := intervention_effect[IU, time_index],
        by = IU]

  } else {

    pop[uptake_p <= intervention_uptake[IU, time_index],
        effect := {
          mu <- intervention_effect[IU, time_index]
          qbeta(p = effect_p,
                shape1 = mu * intervention_effect_var,
                shape2 = (1 - mu) * intervention_effect_var)
        },
        by = IU]

  }
  pop[, rel_cr_eff := rel_contact_rate * sqrt(effect)]
}


### END OF CODE ### ------------------------------------------------------------
