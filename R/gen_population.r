# Generate population of agents ------------------------------------------------
#' Generate population of agents
#'
#' Generate a population of agents. Argument values can be passed down via
#' \code{...} in the \code{virsim} function.
#'
#' @param n_agent Number of agents to be generated.
#' @param n_cluster Number of clusters to be generated.
#' @param n_supercluster Number of superclusters to be generated.
#' @param cluster_size_sd Standard deviation of the distribution of the
#'   logarithm of the expected cluster sizes. As \code{cluster_size_sd}
#'   approaches zero, the cluster size distribution becomes more Poisson-like.
#'   If \code{cluster_size_sd = 0}, then all cluster sizes are exactly equal.
#' @param supercluster_size_sd Standard deviation of the distribution of the
#'   logarithm of the expected supercluster sizes. As
#'   \code{supercluster_size_sd} approaches zero, the cluster size distribution
#'   becomes more Poisson-like. If \code{cluster_size_sd = 0}, then all cluster
#'   sizes are exactly equal.
#' @param contact_shape Shape and rate parameter of gamma distribution (with mean
#'   1.0) for inter-individual variation in contac rate to other agents.
#' @param exposed_time Value or distribution for the time (days) until an
#'   infected individual becomes symptomatic. Define as a list object with named
#'   elements "name" (e.g. "gamma", "unif", "weibull", "constant") and whichever
#'   parameter names (e.g. "shape" and "rate") and values are required by the
#'   corresponding native R functions (e.g. "rgamma", "runif", "rweibull"). If
#'   \code{name = "constant"}, no additional elements are required.
#' @param infected_time Value or distribution for the duration of symptoms
#'   (days). Define as a list object with named elements "name" (e.g. "gamma",
#'   "unif", "weibull", "constant") and whichever parameter names (e.g. "shape"
#'   and "rate") and values are required by the corresponding native R functions
#'   (e.g. "rgamma", "runif", "rweibull"). If \code{name = "constant"}, no
#'   additional elements are required.
#' @param SC_IU_link A numeric value between zero and one indicating the
#'   strength of correlation between the definition of superclusters (SC) where
#'   transmission is clustered and the inmplementation unit (IU) where
#'   interventions are implemented. If set to 1.0, SCs and IUs are identical; if
#'   set to 0.5, an IU will be guaranteed to cover 50% of the clusters of a
#'   single supercluster and the other 50% will be clusters from random
#'   superclusters (including the one that is guaranteed to be covered); if set
#'   to 0, the overlap of SCs and IUs is completely random.
#' @param ... Arguments passed down via \code{...} in the \code{virsim}
#'   function.
#'
#' @return Returns a \code{data.table} of agents.
#'
#' @import data.table
#'
#' @export
gen_population <- function(n_agent,
                           n_cluster,
                           n_supercluster,
                           cluster_size_sd,
                           supercluster_size_sd,
                           contact_shape,
                           contact_assort,
                           exposed_time,
                           infected_time,
                           SC_IU_link,
                           ...) {

  if (SC_IU_link > 1 || SC_IU_link < 0) {
    stop("SC_IU_link must be >=0 and <=1", call. = FALSE)
  }
  if (n_agent < n_cluster) {
    warning("n_agent < n_cluster", call. = FALSE)
  }
  if (n_agent < n_supercluster) {
    warning("n_agent < n_supercluster", call. = FALSE)
  }
  if (n_cluster < n_supercluster) {
    warning("n_cluster < n_supercluster", call. = FALSE)
  }

  # Generate cluster sizes (discard clusters of zero size)
  if(cluster_size_sd > 0) {
    cluster_size_rel <- exp(rnorm(n = n_cluster,
                                  mean = 0,
                                  sd = cluster_size_sd))
    cluster_size <- c(rmultinom(n = 1,
                                size = n_agent,
                                prob = cluster_size_rel))
  } else {
    cluster_size <- rep(floor(n_agent / n_cluster), n_cluster)
    if (n_agent - sum(cluster_size) > 0) {
      cluster_size[1:(n_agent - sum(cluster_size))] <-
        cluster_size[1:(n_agent - sum(cluster_size))] + 1
    }
  }
  cluster_size <- cluster_size[cluster_size > 0]
  if (n_cluster > length(cluster_size)) {
    warning(paste0(n_cluster - length(cluster_size),
                   " out of ", n_cluster,
                   " clusters with zero population"),
            call. = FALSE)
  }
  n_cluster <- length(cluster_size)

  # Generate supercluster sizes
  if(supercluster_size_sd > 0) {
    supercluster_size_rel <- exp(rnorm(n = n_supercluster,
                                       mean = 0,
                                       sd = supercluster_size_sd))
    supercluster_size <- c(rmultinom(n = 1,
                                     size = n_cluster,
                                     prob = supercluster_size_rel))
  } else {
    supercluster_size <- rep(floor(n_cluster / n_supercluster), n_supercluster)
    if (n_cluster - sum(supercluster_size) > 0) {
      supercluster_size[1:(n_cluster - sum(supercluster_size))] <-
        supercluster_size[1:(n_cluster - sum(supercluster_size))] + 1
    }
  }
  supercluster_size <- supercluster_size[supercluster_size > 0]
  if (n_supercluster > length(supercluster_size)) {
    warning(paste0(n_supercluster - length(supercluster_size),
                   " out of ", n_supercluster,
                   " superclusters with zero population"),
            call. = FALSE)
  }
  n_supercluster <- length(supercluster_size)

  # Assign transmission units (TU)
  pop <- data.table(supercluster = unlist(sapply(1:n_supercluster,
                                                 function(i) {
                                                   rep(i, supercluster_size[i])
                                                 }, simplify = FALSE)),
                    cluster = 1:n_cluster)

  pop[, IU := supercluster]
  if (SC_IU_link < 1) {
    pop[, IU := {

      n_cluster_reassign <- round(.N * (1 - SC_IU_link), 0)

      if(n_cluster_reassign > 0) {
        if(n_cluster_reassign < .N) {
          c(IU[1:(.N - n_cluster_reassign)],
            sample.int(n = n_supercluster,
                       size = n_cluster_reassign,
                       replace = TRUE))
        } else {
          sample.int(n = n_supercluster,
                     size = n_cluster_reassign,
                     replace = TRUE)
        }
      } else {
        IU
      }

    }, by = supercluster]
  }

  # Generate agent population
  pop <- pop[rep(seq(1, .N), cluster_size)]

  # Disease state (SEIR model)
  pop[, state := factor(1, levels = 1:4, labels = c("S", "E", "I", "R"))]
  pop[, time_till_symptoms := as.numeric(NA)]
  pop[, time_till_recovery := as.numeric(NA)]

  # Individual factor for intervention uptake and effect
  pop[, uptake_p := runif(n = n_agent, min = 0, max = 1)]
  pop[, effect_p := runif(n = n_agent, min = 0, max = 1)]

  # Multiplier for change in contact rate due to interventions
  pop[, effect := 1]
  pop[, traced := FALSE]
  pop[, trace_effect := 1]

  # Assign individual (relative) contact rates
  if (contact_shape < Inf) {

    x_individual <- rnorm(n = n_agent, mean = 0, sd = 1)
    x_cluster <- rnorm(n = n_cluster, mean = 0, sd = 1)

    x_cluster <- unlist(sapply(1:n_cluster,
                               function(i) rep(x_cluster[i],
                                               cluster_size[i]),
                               simplify = FALSE))
    x_total <- contact_assort * x_cluster + (1 - contact_assort) * x_individual

    contact_rate_p <- sort(runif(n_agent))[rank(x_total)]

    pop[, rel_contact_rate := qgamma(p = contact_rate_p,
                                     shape = contact_shape,
                                     rate = contact_shape)]

  } else {

    pop[, rel_contact_rate := 1]

  }

  pop[, rel_cr_eff := rel_contact_rate * sqrt(effect)]

  # Assign random durations of disease states (for if and when they occur)
  pop[, E_duration := do.call(what = paste0("r", exposed_time$name),
                              args = c(list(n = .N),
                                            exposed_time[-1]))]
  pop[, I_duration := do.call(what = paste0("r", infected_time$name),
                              args = c(list(n = .N),
                                       infected_time[-1]))]

  return(pop)
}


### END OF CODE ###

