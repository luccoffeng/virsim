# virsim

<!-- badges: start -->
<!-- badges: end -->

The goal of virsim is to provide a user-friendly interface to perform stochastic simulations of transmission and control of microparasitic infections (e.g. viruses and bacteria) in a geographically or socially structured population. A technical description of the model can be found in De Vlas & Coffeng (2021), which can be found [here](https://doi.org/10.1038/s41598-021-83492-7) (see Supplement 1 for technical description).

## Installation
Before installing virsim, make sure that you have set up the c++ toolchain for R on your operating system. For Windows OS, this means that you should install Rtools. For Macs, install Xcode (and don't forget to agree to the license agreement).

You can install the latest released version of virsim from GitLab:

``` r
remotes::install_gitlab("luccoffeng/virsim")
```

The development version can be downloaded by:

``` r
remotes::install_gitlab("luccoffeng/virsim@dev")
```


## Example
``` r
rm(list = ls())
library(virsim)
library(data.table)

test <- virsim(runtime = 20)
# or muck around with parameter values (see ?virsim and ?param_sim for parameters that are commonly manipulated)
test <- virsim(runtime = 20, n_agent = 1e3, n_cluster = 1, n_supercluster = 1)

# calculate IC admissions based on parameterisation for the first Covid-19 wave in the Netherlands
test <- virsim(runtime = 50)
test$monitor[, c("IC_inc", "IC_prev") := gen_derived_outcome(inc = inc, time = time)]

```
