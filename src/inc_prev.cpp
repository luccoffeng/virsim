#include <RcppArmadillo.h>
// [[Rcpp::depends("RcppArmadillo")]]

using namespace Rcpp;
using namespace arma;

//' @importFrom Rcpp evalCpp
//' @useDynLib virsim, .registration = TRUE
//' @export
// [[Rcpp::export]]
arma::mat inc_prev(arma::vec time,
                   arma::vec start_outcome,
                   arma::vec end_outcome) {
  int n = time.n_rows;
  arma::mat out(n, 2);
  for (int i = 0; i < n; i++) {
    arma::vec tt = time.at(i) - start_outcome;
    arma::vec zz = time.at(i) - end_outcome;
    out.at(i, 0) = arma::sum(tt > 0 && tt < 1);
    out.at(i, 1) = arma::sum(tt > 0 && zz < 0);
  }
  return(out);
}
